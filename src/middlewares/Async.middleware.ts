import { Request, NextFunction, Response } from 'express'
import { AsyncRequestHandler } from 'interfaces/Middleware.interface'

export function wrapAsync (fn: AsyncRequestHandler) {
  return (req: Request, res: Response, next: NextFunction) => {
    fn(req, res, next)
      .catch(err => next(err))
  }
}
