import { Request, Response } from 'express'
import { BadRequest, ApiError } from 'utils/Error.utils'

export function handleError (err: ApiError, _req: Request, res: Response): void {
  if (err instanceof BadRequest) {
    res.status(400).json(err.details.errors)
  } else {
    res.status(500).json({ message: 'An error has occurred' })
  }
}
