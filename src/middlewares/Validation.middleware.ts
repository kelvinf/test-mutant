import { NextFunction, Request, Response } from 'express'
import { BadRequest } from 'utils/Error.utils'
import Ajv = require('ajv')

export function validateMiddleware (schema: object, property = 'body') {
  return (req: Request, _res: Response, next: NextFunction) => {
    const ajv = new Ajv()
    const validate = ajv.compile(schema)
    const valid = validate(req[property]) as boolean
    if (valid) {
      return next()
    }
    next(new BadRequest('Um erro ocorreu', validate))
  }
}
