export const UserRequestSchema = {
  type: 'object',
  properties: {
    filter: {
      type: 'array',
      items: { type: 'string' }
    },
    fields: {
      type: 'array',
      items: { type: 'string' }
    }

  }
}
