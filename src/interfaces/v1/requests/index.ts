export interface RequestOptions<T> {
  fields?: Array<keyof T>
  order?: keyof T
}
