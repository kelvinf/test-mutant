export interface UserRepositoryResponse {
  id: number
  name: string
  username: string
  email: string
  address: {
    street: string
    suite: string
    city: string
    zipcode: string
    geo: {
      lat: string
      lng: string
    }
  }
  phone: string
  website: string
  company: {
    name: string
    catchPhrase: string
    bs: string
  }
}

export interface UserServiceResponse {
  id?: number
  name?: string
  username?: string
  email?: string
  phone?: string
  website?: string
}

export class UserResponse {
  public id?: number
  public name?: string
  public username?: string
  public email?: string
  public phone?: string
  public website?: string

  constructor (obj: any) {
    this.id = obj.id
    this.name = obj.name
    this.username = obj.username
    this.email = obj.email
    this.phone = obj.phone
    this.website = obj.website
  }
}
