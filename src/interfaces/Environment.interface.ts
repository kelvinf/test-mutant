export interface Environment {
  SERVER_PORT: number
  TYPESCRIPT: boolean
}
