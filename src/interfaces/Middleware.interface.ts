import { Request, NextFunction, Response } from 'express'

export type AsyncRequestHandler = (req: Request, res: Response, next: NextFunction) => Promise<void>
