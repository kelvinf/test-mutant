import { Environment } from 'interfaces/Environment.interface'

export const env: Environment = {
  SERVER_PORT: 4000,
  TYPESCRIPT: __filename.slice(-3) === '.ts'
}
