import { Environment } from 'interfaces/Environment.interface'

export const env: Environment = {
  SERVER_PORT: 8080,
  TYPESCRIPT: __filename.slice(-3) === '.ts'
}
