import { env as envProd } from './environment.production'
import { env as envDev } from './environment.development'

export function isProduction (): boolean {
  return process.env.NODE_ENV === 'production'
}

const Environment = isProduction() ? envProd : envDev

export default Environment
