import { Pick } from 'interfaces/utils/Parse.interface'

export function * filterFields<T, Y extends keyof T> (iterator: Iterable<T>, filter: Y[] | undefined): Iterable<Pick<T, Y>> {
  for (const entry of iterator) {
    yield pick(entry, filter)
  }
}

export function pick<T, Y extends keyof T> (oldObj: T, keys: Y[] = []): Pick<T, Y> {
  if (keys.length === 0) {
    return oldObj
  }

  const newObj: any = {}
  keys.forEach(key => {
    newObj[key] = oldObj[key]
  })
  return newObj
}

export async function asyncGeneratorToArray<T, Y = any> (generator: AsyncGenerator<Y>, mapFn?: (entry: Y) => T): Promise<T[]> {
  const list: any[] = []
  for await (const entry of generator) {
    const newObj = mapFn != null ? mapFn(entry) : entry
    list.push(newObj)
  }
  return list
}

export function orderBy<T> (arr: T[], order: string | undefined): T[] {
  if (order == null) {
    return arr
  }
  return arr.sort((a, b) => a[order] - b[order])
}
