export class ApiError extends Error {
  public details: any

  constructor (message: string, details: any) {
    super(message)
    this.details = details
  }
}

export class BadRequest extends ApiError { }
