export function camelCase (text: string): string {
  const textCamelCase = text[0].toLowerCase() + text.slice(1)
  return textCamelCase
}
