import chalk from 'chalk'
import { isProduction } from '../config/environment'
import figlet = require('figlet')
import clear = require('clear')

export function displayStart (port: number): void {
  figlet('Test Mutant', {
    font: 'Ghost'
  }, (err: Error, data: any) => {
    if (err != null) throw err
    clear()
    console.log(chalk.greenBright(data))
    console.log(`Running on port: ${chalk.bgMagenta.black(` ${port} `)}`)
    if (isProduction()) {
      console.log(chalk.bold.bgRed.white(' PRODUCTION '))
    }
  })
}
