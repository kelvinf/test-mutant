import { camelCase } from '../../utils/String.utils'
import controllers = require('../controllers')
import express = require('express')

const RoutesV1 = express.Router()

Object.keys(controllers)
  .forEach(controllerName => {
    const routerName = '/' + camelCase(controllerName)
    const controller = controllers[controllerName]
    RoutesV1.use(routerName, controller)
  })

export default RoutesV1
