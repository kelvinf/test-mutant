import { UserServiceResponse } from 'interfaces/v1/responses/UserResponse.interface'
import { RequestOptions } from 'interfaces/v1/requests'
import UserRepository = require('v1/repositories/User.repository')

export async function getAll (options: RequestOptions<UserServiceResponse> = {}): Promise<Iterable<UserServiceResponse>> {
  return UserRepository.getAll(options)
}
