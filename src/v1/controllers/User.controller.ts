import { wrapAsync } from 'middlewares/Async.middleware'
import { UserResponse } from 'interfaces/v1/responses/UserResponse.interface'
import { validateMiddleware } from 'middlewares/Validation.middleware'
import { UserRequestSchema } from 'interfaces/v1/requests/UserRequest.interface'
import express = require('express')
import userService = require('v1/services/User.service')

const router = express.Router()

router.post(
  '/pick',
  validateMiddleware(UserRequestSchema),
  wrapAsync(async (req, res) => {
    const { fields } = req.body
    const users = await userService.getAll(fields)
    const response = Array.from(users, entry => new UserResponse(entry))
    res.send(response)
  })
)

export = router
