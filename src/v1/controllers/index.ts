/* eslint-disable  @typescript-eslint/no-var-requires */
import fs = require('fs')
import path = require('path')
const suffix = '.controller.ts'
const pattern = new RegExp(`${suffix}$`)

const ControllersV1 = {}
fs.readdirSync(__dirname)
  .filter(file => {
    return file.indexOf('.') !== 0 && pattern.test(file)
  })
  .forEach(file => {
    const controllerName = file.slice(0, -suffix.length)
    const controllerPath = path.join(__dirname, file)
    const controller = require(controllerPath)
    ControllersV1[controllerName] = controller
  })

export = ControllersV1
