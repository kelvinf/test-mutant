import Axios from 'axios'
import { filterFields, orderBy } from 'utils/Parse.utils'
import { UserRepositoryResponse } from 'interfaces/v1/responses/UserResponse.interface'
import { RequestOptions } from 'interfaces/v1/requests'

const userRequest = Axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/users'
})

export async function getAll (options: RequestOptions<UserRepositoryResponse> = {}): Promise<Iterable<UserRepositoryResponse>> {
  const { data } = await userRequest.get<UserRepositoryResponse[]>('/')
  const { fields, order } = options
  const ordered = orderBy(data, order)
  const result = filterFields(ordered, fields)
  return result
}
