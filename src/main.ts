import { displayStart } from './utils/Console.utils'
import Environment from './config/environment'
import RoutesV1 from './v1/routes'
import { handleError } from 'middlewares/Error.midleware'
import express = require('express')
const { SERVER_PORT } = Environment

express()
  .use(express.json())
  .use('/v1', RoutesV1)
  .use(handleError)
  .listen(SERVER_PORT, () => displayStart(SERVER_PORT))
